package ni.training.android.wasap;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.IOException;
import java.io.InputStream;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentoDinamico.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentoDinamico#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentoDinamico extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ID_IMAGEN = "ID_IMG";

    // TODO: Rename and change types of parameters
    private int idImgActual= 1;

    private OnFragmentInteractionListener mListener;
    private TextView textView;
    private VideoView vid;

    public FragmentoDinamico() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentoDinamico.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentoDinamico newInstance() {
        FragmentoDinamico fragment = new FragmentoDinamico();
//        Bundle args = new Bundle();
//        args.putInt(ID_IMAGEN, param1);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //Cargamos la imagen o lo que tengamos que cargar
        View v = inflater.inflate(R.layout.fragment_fragmento_dinamico, container, false);
        Button btSiguiente = (Button)v.findViewById(R.id.btn_siguiente);
        vid = (VideoView)v.findViewById(R.id.video);
        btSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                idImgActual++;
                actualizarVideo();
            }
        });
        Button btnAnterior = (Button)v.findViewById(R.id.btn_ant);
        btnAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (idImgActual<0)idImgActual++;
                else idImgActual--;
                actualizarVideo();
            }
        });
        textView = (TextView)v.findViewById(R.id.nombreImagen);


        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


//  METODOS DECLARADOS POR EL PROGRAMADOR
    private void actualizarVideo() {
        textView.setText("Viendo el video #"+idImgActual);
        vid.setVideoPath("/url/al/video/"+idImgActual+".mp4");
    }

}
