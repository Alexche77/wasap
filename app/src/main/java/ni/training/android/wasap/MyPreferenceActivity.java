package ni.training.android.wasap;

import android.content.SharedPreferences;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MyPreferenceActivity extends AppCompatActivity implements  SharedPreferences.OnSharedPreferenceChangeListener{
    Toolbar toolbar;

    public static final String TAG = MyPreferenceActivity.class.getSimpleName();
    private View areaPerfil;
    private boolean viendoPerfil = false;
    private TextView txtUser, txtStatus;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Configuramos una activity con un layout que tenga toolbar
        setContentView(R.layout.config_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        cargarDatosPerfil();
        //Cargamos el fragment
        getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new MyPreferenceFragment()).commit();

//        Para escuchar los cambios
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        prefs.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(viendoPerfil){
            actualizarGUI();
        }
    }

    private void actualizarGUI() {
        areaPerfil.setVisibility(View.VISIBLE);
        toolbar.setTitle(getString(R.string.title_activity_configuraciones));
        viendoPerfil = false;
    }

    private void cargarDatosPerfil() {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String strUserName = SP.getString(Constantes.PREFS_USERNAME, getString(R.string.not_set));
        String strStatus = SP.getString(Constantes.PREFS_ESTADO, getString(R.string.not_set));

        txtUser = (TextView)findViewById(R.id.username_txtv);
        txtUser.setText(strUserName);

        txtStatus = (TextView)findViewById(R.id.estado_txtv);
        txtStatus.setText(strStatus);
    }

    public void configPerfil(View view){
        Log.d(TAG, "configPerfil: Abrimos el fragmento para el perfil");
        areaPerfil = findViewById(R.id.perfilArea);
//        Cambiamos el titulo a la toolbar
        toolbar.setTitle(getString(R.string.perfil));
        areaPerfil.setVisibility(View.GONE);
//        Cargamos el fragmento
        getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new PerfilPreferenceFragment()).addToBackStack(MyPreferenceFragment.class.getName()).commit();
        viendoPerfil = true;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(TAG,"Settings key changed: " + key);
        if(key.equals(Constantes.PREFS_USERNAME)){
            txtUser.setText(prefs.getString(key,"Error"));
            Toast.makeText(getBaseContext(),"Se modificó el username",Toast.LENGTH_SHORT).show();
        }

        if(key.equals(Constantes.PREFS_ESTADO))
            txtStatus.setText(prefs.getString(key,"Error"));
        if(key.equals(Constantes.PREF_INTRO_APP))
            Log.d(Constantes.TAG_DEBGGING, "INTRO_APP: "+prefs.getBoolean(key,true));
    }
}
