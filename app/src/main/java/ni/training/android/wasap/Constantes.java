package ni.training.android.wasap;

/**
 * Created by Alvaro on 26/8/2017.
 */

public class Constantes {
    public static final String PREFS_ESTADO = "status";
    public static final String PREFS_USERNAME = "username";
    public static final String TAG_DEBGGING = "FIJATE_AQUI";
    public static final String PREF_INTRO_APP = "INTRO_APP";
}
